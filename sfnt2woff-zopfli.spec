Name:           sfnt2woff-zopfli
Version:        1.1.0
Release:        1%{?dist}
Summary:        Create WOFF files with Zopfli compression

# From README.md:
#   The WOFF portion of the code is taken from sfnt2woff, which is licensed
#   under the MPL/GPL/LGPL. The Zopfli implementation is licensed under the
#   Apache License. My modifications to woff.c are also licensed under the
#   MPL/GPL/LGPL.
# Note that all bundled Zopfli code is removed in prep, so only the
# MPL/GPL/LGPL portions remain in the build RPM.
License:        MPLv1.0 or GPLv2+ or LGPLv2+
URL:            https://github.com/bramstein/%{name}
Source0:        https://github.com/bramstein/%{name}/archive/v%{version}.tar.gz
# The source tarball contains a license file, but it does not reproduce the
# full upstream WOFF license text, nor does it include the text of the three
# referenced licenses. See upstream issue
# https://github.com/bramstein/sfnt2woff-zopfli/issues/11 and PR
# https://github.com/bramstein/sfnt2woff-zopfli/pull/10.
# We could get away without distributing the MPL/GPL/LGPL full text, but we
# certainly do need the complete upstream WOFF license text. See:
#   https://docs.fedoraproject.org/en-US/packaging-guidelines/
#       LicensingGuidelines/#_license_text
Source1:        https://raw.githubusercontent.com/samboy/WOFF/2017-06-11/LICENSE
Source2:        https://www.mozilla.org/media/MPL/1.1/index.0c5913925d40.txt
Source3:        https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
Source4:        https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt
# Man pages submitted upstream:
#   https://github.com/bramstein/sfnt2woff-zopfli/issues/12
Source5:        sfnt2woff-zopfli.1
Source6:        woff2sfnt-zopfli.1
# Patched Makefile supporting external Zopfli library; see
# https://github.com/bramstein/sfnt2woff-zopfli/issues/11 and PR
# https://github.com/bramstein/sfnt2woff-zopfli/pull/10.
Source7:        https://raw.githubusercontent.com/musicinmybrain/sfnt2woff-zopfli/6b63776f3d03f8b88d23cedb6fe2a9bd2947d93d/Makefile

# Use only Zopfli public API functions from zopfli.h.
#   
# The Zopfli library is normally installed only with zopfli.h (and
# zopflipng_lib.h). Using only APIs in this header makes it easier to use an
# external Zopfli library. This means including zopfli.h instead of
# zlib_container.h, and calling ZopfliCompress with ZOPFLI_FORMAT_ZLIB instead
# of calling ZopfliZlibCompress directly.
#
# See upstream issue https://github.com/bramstein/sfnt2woff-zopfli/issues/11;
# this patch is from https://github.com/bramstein/sfnt2woff-zopfli/pull/10:
#   https://github.com/bramstein/sfnt2woff-zopfli/pull/10/commits/9ed2430
Patch1:         %{name}-use-public-api.patch

BuildRequires:  gcc
BuildRequires:  pkgconfig(zlib)
BuildRequires:  zopfli-devel


%description
This is a modified version of the sfnt2woff utility that uses Zopfli as a
compression algorithm instead of zlib. This results in compression gains of —
on average — 5-8% compared to regular WOFF files. Zopfli generates compressed
output that is compatible with regular zlib compression so the resulting WOFF
files can be used everywhere.


%prep
%autosetup -p1

cp -p %{SOURCE1} LICENSE-WOFF
cp -p %{SOURCE2} LICENSE-WOFF-MPL
cp -p %{SOURCE3} LICENSE-WOFF-GPL
cp -p %{SOURCE4} LICENSE-WOFF-LGPL

# Strip out bundled Zopfli sources
rm -f \
    blocksplitter.? \
    cache.? \
    deflate.? \
    gzip_container.? \
    hash.? \
    katajainen.? \
    lz77.? \
    squeeze.? \
    tree.? \
    util.? \
    zlib_container.? \
    zopfli.h \
    zopfli_bin.c \
    zopfli_lib.c
if grep -ErinI --exclude=LICENSE --exclude=README.md 'apache|google' .
then
  echo 'Possibly missed removing a bundled Zopfli source file' 1>&2
  exit 1
fi


%build
%set_build_flags
%make_build -f %{SOURCE7} \
    ZLIB_CFLAGS="$(pkg-config --cflags zlib)" \
    ZLIB_LIBS="$(pkg-config --libs zlib)" \
    ZOPFLI_CFLAGS='' \
    ZOPFLI_LIBS='-lzopfli'


%install
install -d '%{buildroot}%{_bindir}'
for bin in sfnt2woff woff2sfnt
do
  install -t '%{buildroot}%{_bindir}' -p "${bin}-zopfli"
done
install -d '%{buildroot}%{_mandir}/man1'
install -t '%{buildroot}%{_mandir}/man1' -p -m 0644 '%{SOURCE5}' '%{SOURCE6}'


%files
%doc README.md
%license LICENSE
%license LICENSE-WOFF
%license LICENSE-WOFF-MPL
%license LICENSE-WOFF-GPL
%license LICENSE-WOFF-LGPL

%{_bindir}/sfnt2woff-zopfli
%{_bindir}/woff2sfnt-zopfli

%{_mandir}/man1/sfnt2woff-zopfli.1*
%{_mandir}/man1/woff2sfnt-zopfli.1*


%changelog
* Thu Nov 12 2020 Benjamin A. Beasley <code@musicinmybrain.net> - 1.1.0-1
- Initial spec file
